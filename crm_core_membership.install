<?php

/**
 * @file
 * Install, update and uninstall functions for the CRM Core membership module.
 */

/**
 * Implements hook_schema().
 */
function crm_core_membership_schema() {
  $schema['crm_core_membership_type'] = array(
    'description' => 'Stores information about all defined membership types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique membership type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'translatable' => TRUE,
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
      'enabled' => array(
        'description' => 'A boolean indicating whether this type is enabled.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
      'locked' => array(
        'description' => 'A boolean indicating whether this type is locked or not, locked contact type cannot be edited or disabled/deleted',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
      'term_interval' => array(
        'description'   => 'The number of multiples of the period for membership term.',
        'type'          => 'int',
        'size'          => 'medium',
        'not null'      => TRUE,
        'default'       => 1,
      ),
      'term_period' => array(
        'description'   => 'The period machine name of the membership term.',
        'type'          => 'varchar',
        'size'          => 'normal',
        'length'        => 20,
        'not null'      => TRUE,
        'default'       => 'year',
      ),
      'grace_interval' => array(
        'description'   => 'The number of multiples of the grace period.',
        'type'          => 'int',
        'size'          => 'medium',
        'not null'      => TRUE,
        'default'       => 1,
      ),
      'grace_period' => array(
        'description'   => 'The period machine name for the grace period.',
        'type'          => 'varchar',
        'size'          => 'normal',
        'length'        => 20,
        'not null'      => TRUE,
        'default'       => 'month',
      ),
      'price_new' => array(
        'type' => 'numeric',
        'unsigned' => TRUE,
        'scale' => 2,
        'precision' => 10,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Price for a new membership.',
      ),
      'price_renew' => array(
        'type' => 'numeric',
        'unsigned' => TRUE,
        'scale' => 2,
        'precision' => 10,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Price for renewing a membership',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function crm_core_membership_install() {
  // Extends 'crm_core_activity' table with 'state' field to simplify
  // search of memberships in different states.
  if (!db_field_exists('crm_core_activity', 'state')) {
    $schema = drupal_get_schema('crm_core_activity', TRUE);
    db_add_field('crm_core_activity', 'state', $schema['fields']['state']);
  }
  // Set predefined membership states.
  $states = array(
    'cmcm_created' => 'New',
    'cmcm_renewed' => 'Renewed',
    'cmcm_grace' => 'Grace period',
    'cmcm_expired' => 'Expired',
    'cmcm_cancelled' => 'Cancelled',
  );
  variable_set('crm_core_membership_states', $states);
}

/**
 * Implements hook_uninstall().
 */
function crm_core_membership_uninstall() {
  // Drop state field in crm_core_activity table.
  if (db_field_exists('crm_core_activity', 'state')) {
    db_drop_field('crm_core_activity', 'state');
  }

  // Variables cleanup.
  variable_del('crm_core_membership_states');
  variable_del('crm_core_membership_cron_last_run');
}
