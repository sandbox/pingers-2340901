<?php
/*
 * @file
 * Line chart.
 * Displays a chart containing donation totals by day for a specific source.
 * TODO: Replace CRM Core Donation presets with CRM Core Membership presets
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Membership Level: Activity by Year'),
  'description' => t('Displays a line chart displaying new memberships for the selected level.'),
  'category' => t('CRM Core Membership'),
  'edit form' => 'crm_core_membership_level_activity_chart_form',
  'render callback' => 'crm_core_membership_level_activity_chart',
  'admin info' => 'crm_core_membership_level_activity_chart_info',
  'defaults' => array(
    'days_to_display' => 'All',
    'height' => '',
    'xaxis_tickmarks' => '',
    'chart_color' => '',
    'line_color' => '',
    'background_color' => '',
    'include_legend' => 0,
    'include_tooltip' => 0,
  ),
  'all contexts' => TRUE,
);

/**
 * admin info
 */
function crm_core_membership_level_activity_chart_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'Membership Level: Activity by Year';
    $block->content = t('Displays a line chart displaying annual membership levels.');
    return $block;
  }
}

/**
 * Settings form
 */
function crm_core_membership_level_activity_chart_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['days_to_display'] = array(
    '#type' => 'textfield',
    '#title' => t('Days to display'),
    '#description' => t('Please enter the number of days to display in this chart. Use the value All to display all values for this source.'),
    '#default_value' => !empty($conf['days_to_display']) ? $conf['days_to_display'] : '90',
  );

  $form['include_tooltip'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display tooltips'),
    '#description' => t('Check this box to display tooltips when hovering over a point in your chart.'),
    '#default_value' => !empty($conf['include_tooltip']) ? $conf['include_tooltip'] : 0,
    '#return_value' => 1,
  );

  $form['include_legend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display a legend'),
    '#description' => t('Check this box to include a legend in your chart.'),
    '#default_value' => !empty($conf['include_legend']) ? $conf['include_legend'] : '0',
    '#return_value' => 1,
  );

  // display settings
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );

  // height
  $form['display_settings']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Please enter the height of the chart, as an integer.'),
    '#default_value' => !empty($conf['height']) ? $conf['height'] : '',
  );

  // line color
  $form['display_settings']['line_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Line color'),
    '#description' => t('Please enter the color to use for the line in valid #RRGGBB or rgba format. Leave blank to use the default color.'),
    '#default_value' => !empty($conf['line_color']) ? $conf['line_color'] : '',
  );

  // chart color
  $form['display_settings']['chart_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart color'),
    '#description' => t('Please enter the color to use for the line in valid #RRGGBB or rgba format. Leave blank to use the default color.'),
    '#default_value' => !empty($conf['chart_color']) ? $conf['chart_color'] : '',
  );

  // series background color
  $form['display_settings']['background_color'] = array(
    '#type' => 'textarea',
    '#title' => t('Background color'),
    '#description' => t('Please enter the color to use for the background of the series in valid #RRGGBB or rgba format. Leave blank to use the default color. You can use gradients for this, separate each value with a new line.'),
    '#default_value' => !empty($conf['background_color']) ? $conf['background_color'] : '',
  );

  $form['display_settings']['xaxis'] = array(
    '#type' => 'fieldset',
    '#title' => t('X-axis settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['display_settings']['xaxis']['xaxis_tickmarks'] = array(
    '#type' => 'textfield',
    '#title' => t('Tick marks'),
    '#description' => t('Please enter the number of tickmarks to display on the xaxis.'),
    '#default_value' => !empty($conf['xaxis_tickmarks']) ? $conf['xaxis_tickmarks'] : '',
  );

  return $form;
}

/**
 * Validation handler
 */
function crm_core_membership_level_activity_chart_form_validate($form, &$form_state) {

  // timeframe
  if (!is_numeric($form_state['values']['days_to_display']) && $form_state['values']['days_to_display'] !== 'All') {
    form_set_error('days_to_display', t('The number of days must be entered as a number (i.e. 90). You can also enter \'All\' to display all records for this source.'));
  }

  // xaxis
  if (!empty($form_state['values']['xaxis_tickmarks']) && !is_numeric($form_state['values']['xaxis_tickmarks'])) {
    form_set_error('xaxis_tickmarks', t('The number of tickmarks for the xaxis must be entered as a number (i.e. 7).'));
  }

  // height
  if (!empty($form_state['values']['height']) && !is_numeric($form_state['values']['height'])) {
    form_set_error('height', t('The height of the chart must be entered as a number (i.e. 200).'));
  }
}

/**
 * Submission handler
 */
function crm_core_membership_level_activity_chart_form_submit($form, &$form_state) {

  // timeframe
  $form_state['conf']['days_to_display'] = $form_state['values']['days_to_display'];

  // display tooltip
  $form_state['conf']['include_tooltip'] = $form_state['values']['include_tooltip'];

  // legend
  $form_state['conf']['include_legend'] = $form_state['values']['include_legend'];

  // height
  $form_state['conf']['height'] = $form_state['values']['height'];

  // xaxis
  $form_state['conf']['xaxis_tickmarks'] = $form_state['values']['xaxis_tickmarks'];

  // colors
  $form_state['conf']['line_color'] = $form_state['values']['line_color'];
  $form_state['conf']['chart_color'] = $form_state['values']['chart_color'];
  $form_state['conf']['background_color'] = $form_state['values']['background_color'];
}

/**
 * Render callback.
 */
function crm_core_membership_level_activity_chart($subtype, $conf, $panel_args, $context = NULL) {
  $membership_type_id = $context['argument_entity_id:crm_core_membership_type_1']->argument;

  // Store the year and the totals in an array.
  $year_totals = array();

  // First, run a query to get all the years
  // get membership records with the right level
  // get start date for each one
  // start date is stored in the membership record
  // level is stored in the membership type field.
  $year_sql = "
    SELECT DISTINCT YEAR(field_cmcm_start_date_value) as year
    FROM {field_data_field_cmcm_start_date} fad
      JOIN {field_data_field_cmcm_membership_type} fmt
      ON fad.entity_id = fmt.entity_id
    WHERE
      fad.entity_type = 'crm_core_activity'
      AND fad.bundle = 'cmcm_membership'
      AND field_cmcm_membership_type_target_id = :level
    ORDER BY year";

  // Get all the years.
  $years = db_query($year_sql, array(':level' => $membership_type_id));

  foreach ($years as $year) {
    // Get the total number of memberships for that year
    // we should be selecting anything with a start date
    // before or during that year and an end date after that year.
    $total_sql = "
      SELECT COUNT(fmt.entity_id) as total
      FROM {field_data_field_cmcm_start_date} fad
        JOIN {field_data_field_cmcm_membership_type} fmt
        ON fad.entity_id = fmt.entity_id
        JOIN {field_data_field_cmcm_end_date} fed
        ON fad.entity_id = fed.entity_id
      WHERE
        fad.entity_type = 'crm_core_activity'
        AND fad.bundle = 'cmcm_membership'
        AND field_cmcm_membership_type_target_id = :level
        AND YEAR(fad.field_cmcm_start_date_value) = :year
        AND YEAR(fed.field_cmcm_end_date_value) >= :year
      ";

    // Get totals for this year.
    $totals = db_query($total_sql, array(
      ':level' => $membership_type_id,
      ':year' => $year->year,
    ));

    foreach ($totals as $total) {
      $year_totals[$year->year] = array(
        'year' => $year->year,
        'totals' => $total->total,
      );
    }
  }

  // Prepare the series.
  $date_item = array();
  $last_date = '';
  $last_date_ts = '';
  $series_labels = array();
  $ticks = array();

  foreach ($year_totals as $year_total) {
    $date = new DateObject('1 Jan ' . $year_total['year']);
    $ts = $date->format('U');
    $date_item[] = array($ts * 1000, $year_total['totals']);
    $ticks[] = $ts * 1000;
    // Record a label.
    $series_labels[] = $year_total['year'] . '<br />' . $year_total['totals'];
    // Set last date.
    $last_date = $year_total['year'];
    $last_date_ts = $ts;
  }

  $date_data = new flotData($date_item);
  $date_data->label = t('Membership by year');

  $data[] = $date_data;

  // Configuration.
  // If setting not set for widget, we grabbing it from modules configuration.
  $height = empty($conf['height']) ? variable_get('crm_core_donation_height', '200') : $conf['height'];
  $xaxis_tickmarks = empty($conf['xaxis_tickmarks']) ? variable_get('crm_core_donation_xaxis_tickmarks', '7') : $conf['xaxis_tickmarks'];
  $chart_color = empty($conf['chart_color']) ? variable_get('crm_core_donation_chart_color', '') : $conf['chart_color'];
  $line_color = empty($conf['line_color']) ? variable_get('crm_core_donation_line_color', '') : $conf['line_color'];
  $background_color = empty($conf['background_color']) ? variable_get('crm_core_donation_background_color', '') : $conf['background_color'];
  $include_legend = empty($conf['include_legend']) ? variable_get('crm_core_donation_include_legend', 0) : $conf['include_legend'];
  $include_tooltip = empty($conf['include_tooltip']) ? variable_get('crm_core_donation_include_tooltips', 0) : $conf['include_tooltip'];

  // Create a new line chart.
  $options = new flotStyleLine();

  // Add points to the chart.
  $options->series->points = new stdClass();
  $options->series->useLabel = 0;
  $options->series->points->show = TRUE;
  $options->series->points->fill = TRUE;
  $options->series->points->radius = 4;
  $options->series->points->lineWidth = 2;

  // Add lines to the chart.
  $options->series->lines = new stdClass();
  $options->series->lines->show = TRUE;
  $options->series->lines->fill = TRUE;
  $options->series->lines->lineWidth = 4;

  // Add colors to the line.
  if (!empty($line_color)) {
    $options->colors = array(
      $line_color,
    );
  }

  // Add a background for the series.
  if (!empty($background_color)) {
    $options->series->lines->fillColor = array(
      'colors' => explode("\n", $background_color),
    );
  }

  // Add a grid.
  $options->grid->borderWidth = 2;
  $options->grid->labelMargin = 8;
  if (!empty($chart_color)) {
    $options->grid->backgroundColor = $chart_color;
  }

  // Add an x-axis.
  $options->xaxis = new stdClass();
  $options->xaxis->mode = "time";
  $options->xaxis->ticks = $ticks;
  $options->xaxis->timeformat = '%y';

  // Add a y-axis.
  $options->yaxis = new stdClass();
  $options->yaxis->min = 0;
  $options->yaxis->minTickSize = 1;
  $options->yaxis->tickDecimals = 0;
  $options->series->seriesLabels = $series_labels;

  // Add labels to the chart.
  $options->seriesLabels = $series_labels;

  // Add the tooltips.
  if (!empty($include_tooltip)) {
    $options->series->show_tooltip = TRUE;
  }
  else {
    $options->series->show_tooltip = FALSE;
  }

  // Return the chart.
  $block = new stdClass();

  if ($conf['override_title'] != 0 && $conf['override_title_text'] != '') {
    $chart_title = $conf['override_title_text'];
  }
  else {
    $chart_title = t('Activity by year');
  }

  $block->title = $chart_title;

  $block->content = theme('flot_graph', array(
    'data' => $data,
    'options' => $options,
    'element' => array(
      'style' => 'width: 100%; height: ' . (int) $height . 'px;',
    ),
    'legend' => ($include_legend === 1) ? TRUE : FALSE,
  ));

  return $block;
}
