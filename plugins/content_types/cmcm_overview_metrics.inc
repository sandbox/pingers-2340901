<?php
/**
 * @file
 * Overview metrics.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Membership overview metrics'),
  'description' => t('Displays a metrics table for overview page.'),
  'category' => t('CRM Core Membership'),
  'render callback' => 'crm_core_membership_overview_metrics_table_render',
  'all contexts' => TRUE,
);

/**
 * Render callback.
 */
function crm_core_membership_overview_metrics_table_render($subtype, $conf, $panel_args, $context = NULL) {
  if (isset($context['argument_entity_id:crm_core_membership_type_1'])) {
    $membership_type_id = $context['argument_entity_id:crm_core_membership_type_1']->argument;
  }
  $block = new stdClass();
  $header = array(
    t('New Memberships(This Month)'),
    t('Total Memberships'),
    t('Expired Memberships'),
  );
  $rows = array();
  $base_query = new EntityFieldQuery();
  $base_query
    ->entityCondition('entity_type', 'crm_core_activity')
    ->entityCondition('bundle', 'cmcm_membership');
  if (isset($membership_type_id)) {
    $base_query->fieldCondition('field_cmcm_membership_type', 'target_id', $membership_type_id);
  }

  // New memberships this month.
  $first_day_of_month = new DateTime('first day of');
  $last_day_of_month = new DateTime('last day of');

  $dates = array(
    $first_day_of_month->format(DATE_FORMAT_DATETIME),
    $last_day_of_month->format(DATE_FORMAT_DATETIME),
  );

  $query = clone $base_query;
  $this_month_total = $query
    ->fieldCondition('field_cmcm_start_date', 'value', $dates, 'BETWEEN')
    ->count()
    ->execute();

  $cells[] = array(
    'data' => $this_month_total,
    'class' => 'memberships-this-month',
  );

  // Total memberships.
  $query = clone $base_query;
  $total = $query
    ->count()
    ->execute();

  $cells[] = array(
    'data' => $total,
    'class' => 'memberships-total',
  );


  // Expired memberships.
  $states = crm_core_membership_states();
  $state = $states['cmcm_expired'];
  $query = clone $base_query;
  $expired = $query
    ->propertyCondition('state', $state)
    ->count()
    ->execute();

  $cells[] = array(
    'data' => $expired,
    'class' => 'memberships-expired',
  );

  $rows[] = $cells;
  $vars = array(
    'header' => $header,
    'rows' => $rows,
  );

  $block->content = theme('table', $vars);


  return $block;
}
