<?php
/*
 * @file
 * Line chart.
 * Displays a chart containing donation totals by day for a specific source.
 * TODO: Replace CRM Core Donation presets with CRM Core Membership presets
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Membership Level: Annual Activity Table'),
  'description' => t('Displays a table displaying all membership activity for the selected source.'),
  'category' => t('CRM Core Membership'),
  'edit form' => 'crm_core_membership_level_activity_table_form',
  'render callback' => 'crm_core_membership_level_activity_table',
  'admin info' => 'crm_core_membership_level_activity_table_info',
  'defaults' => array(
    'days_to_display' => 'All',
    'height' => '',
    'xaxis_tickmarks' => '',
    'chart_color' => '',
    'line_color' => '',
    'background_color' => '',
    'include_legend' => 0,
    'include_tooltip' => 0,
  ),
  'all contexts' => TRUE,
);

/**
 * admin info
 */
function crm_core_membership_level_activity_table_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'Membership Level: Annual Activity Table';
    $block->content = t('Displays a table displaying all membership activity for the selected source.');
    return $block;
  }
}

/**
 * Settings form
 */
function crm_core_membership_level_activity_table_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['days_to_display'] = array(
    '#type' => 'textfield',
    '#title' => t('Days to display'),
    '#description' => t('Please enter the number of days to display in this chart. Use the value All to display all values for this source.'),
    '#default_value' => !empty($conf['days_to_display']) ? $conf['days_to_display'] : '90',
  );

  $form['include_tooltip'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display tooltips'),
    '#description' => t('Check this box to display tooltips when hovering over a point in your chart.'),
    '#default_value' => !empty($conf['include_tooltip']) ? $conf['include_tooltip'] : 0,
    '#return_value' => 1,
  );

  $form['include_legend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display a legend'),
    '#description' => t('Check this box to include a legend in your chart.'),
    '#default_value' => !empty($conf['include_legend']) ? $conf['include_legend'] : '0',
    '#return_value' => 1,
  );

  // display settings
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );

  // height
  $form['display_settings']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Please enter the height of the chart, as an integer.'),
    '#default_value' => !empty($conf['height']) ? $conf['height'] : '',
  );

  // line color
  $form['display_settings']['line_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Line color'),
    '#description' => t('Please enter the color to use for the line in valid #RRGGBB or rgba format. Leave blank to use the default color.'),
    '#default_value' => !empty($conf['line_color']) ? $conf['line_color'] : '',
  );

  // chart color
  $form['display_settings']['chart_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart color'),
    '#description' => t('Please enter the color to use for the line in valid #RRGGBB or rgba format. Leave blank to use the default color.'),
    '#default_value' => !empty($conf['chart_color']) ? $conf['chart_color'] : '',
  );

  // series background color
  $form['display_settings']['background_color'] = array(
    '#type' => 'textarea',
    '#title' => t('Background color'),
    '#description' => t('Please enter the color to use for the background of the series in valid #RRGGBB or rgba format. Leave blank to use the default color. You can use gradients for this, separate each value with a new line.'),
    '#default_value' => !empty($conf['background_color']) ? $conf['background_color'] : '',
  );

  $form['display_settings']['xaxis'] = array(
    '#type' => 'fieldset',
    '#title' => t('X-axis settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['display_settings']['xaxis']['xaxis_tickmarks'] = array(
    '#type' => 'textfield',
    '#title' => t('Tick marks'),
    '#description' => t('Please enter the number of tickmarks to display on the xaxis.'),
    '#default_value' => !empty($conf['xaxis_tickmarks']) ? $conf['xaxis_tickmarks'] : '',
  );

  return $form;
}

/**
 * Validation handler
 */
function crm_core_membership_level_activity_table_form_validate($form, &$form_state) {

  // timeframe
  if (!is_numeric($form_state['values']['days_to_display']) && $form_state['values']['days_to_display'] !== 'All') {
    form_set_error('days_to_display', t('The number of days must be entered as a number (i.e. 90). You can also enter \'All\' to display all records for this source.'));
  }

  // xaxis
  if (!empty($form_state['values']['xaxis_tickmarks']) && !is_numeric($form_state['values']['xaxis_tickmarks'])) {
    form_set_error('xaxis_tickmarks', t('The number of tickmarks for the xaxis must be entered as a number (i.e. 7).'));
  }

  // height
  if (!empty($form_state['values']['height']) && !is_numeric($form_state['values']['height'])) {
    form_set_error('height', t('The height of the chart must be entered as a number (i.e. 200).'));
  }
}

/**
 * Submission handler
 */
function crm_core_membership_level_activity_table_form_submit($form, &$form_state) {

  // timeframe
  $form_state['conf']['days_to_display'] = $form_state['values']['days_to_display'];

  // display tooltip
  $form_state['conf']['include_tooltip'] = $form_state['values']['include_tooltip'];

  // legend
  $form_state['conf']['include_legend'] = $form_state['values']['include_legend'];

  // height
  $form_state['conf']['height'] = $form_state['values']['height'];

  // xaxis
  $form_state['conf']['xaxis_tickmarks'] = $form_state['values']['xaxis_tickmarks'];

  // colors
  $form_state['conf']['line_color'] = $form_state['values']['line_color'];
  $form_state['conf']['chart_color'] = $form_state['values']['chart_color'];
  $form_state['conf']['background_color'] = $form_state['values']['background_color'];
}

/**
 * Render callback
 */
function crm_core_membership_level_activity_table($subtype, $conf, $panel_args, $context = NULL) {

  // get information about the levels

  // store the year and the totals in an array
  $year_totals = '';

  // first, run a query to get all the years
  // get membership records with the right membership type
  // get start date for each one
  // all activity should be relative to teh core membership record
  $year_sql = "
    SELECT DISTINCT YEAR(field_cmcm_start_date_value) as year
    FROM {field_data_field_cmcm_start_date} fad
      JOIN {field_data_field_cmcm_membership_type} fmt
      ON fad.entity_id = fmt.entity_id
    WHERE
      fad.entity_type = 'crm_core_activity'
      AND fad.bundle = 'cmcm_membership'
      AND field_cmcm_membership_type_target_id = :level
    ORDER BY year";

  // get all the years
  $years = db_query($year_sql, array(
    ':level' => decode_entities($panel_args[0])
  ));

  foreach ($years as $year) {

    // prime the year
    $year_totals[$year->year] = array(
      'created' => 0,
      'renewed' => 0,
      'expired' => 0
    );

    // for each year
    // get the total number of new members
    // get the total number of renewals
    // get the total number of expirations
    // this is 3 different activity types
    // - cmcm_created
    // - cmcm_expired
    // - cmcm_renewed
    // each item should happen within the selected year

    $base_sql = "SELECT COUNT(cca.activity_id) as total,
      YEAR(fad.field_activity_date_value) as year
      FROM crm_core_activity cca
      JOIN field_data_field_activity_date fad
      ON cca.activity_id = fad.entity_id
      JOIN field_data_field_cmcm_membership fmb
      ON cca.activity_id = fmb.entity_id
      JOIN crm_core_activity cca2
      ON fmb.field_cmcm_membership_target_id = cca2.activity_id
      JOIN field_data_field_cmcm_membership_type fmt
      ON cca2.activity_id = fmt.entity_id
      WHERE cca.type = :type
      AND fmt.field_cmcm_membership_type_target_id = :level
      AND YEAR(fad.field_activity_date_value) = :year
      GROUP BY year";

    // get total created for this year
    $created = db_query($base_sql, array(
      ':level' => decode_entities($panel_args[0]),
      ':year' => $year->year,
      ':type' => 'cmcm_created'
    ));

    // get total renewed for this year
    $renewed= db_query($base_sql, array(
      ':level' => decode_entities($panel_args[0]),
      ':year' => $year->year,
      ':type' => 'cmcm_renewed'
    ));

    // get total expired for this year
    $expired= db_query($base_sql, array(
      ':level' => decode_entities($panel_args[0]),
      ':year' => $year->year,
      ':type' => 'cmcm_expired'
    ));

    foreach ($created as $total){
      $year_totals[$year->year]['created'] = $total->total;
    }
    foreach ($renewed as $total){
      $year_totals[$year->year]['renewed'] = $total->total;
    }
    foreach ($expired as $total){
      $year_totals[$year->year]['expired'] = $total->total;
    }
  }

  // table headers
  $header = array(
    t('Year'), t('New'), t('Renewing'), t('Expired')
  );

  // table rows
  foreach ($year_totals as $year => $data) {

    $rows[] = array(
      $year,
      $data['created'],
      $data['renewed'],
      $data['expired']
    );
  }

  // display it
  $content = theme('table', array('header' => $header, 'rows' => $rows));


  // return the chart
  $block = new stdClass();

  if($conf['override_title']  != 0 && $conf['override_title_text'] != ''){
    $chart_title = $conf['override_title_text'];
  } else {
    $chart_title = 'Annual Membership Activity';
  }

  $block->title = $chart_title;

  $block->content = $content;

  return $block;
}
