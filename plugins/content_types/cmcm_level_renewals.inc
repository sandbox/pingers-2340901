<?php
/**
 * @file
 * Displays information about which part of members of this type get renewed.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Membership Level: Renewals'),
  'description' => t('Displays information about which part of members of this type get renewed.'),
  'category' => t('CRM Core Membership'),
  'edit form' => 'cmcm_level_renewals_form',
  'render callback' => 'cmcm_level_renewals_render',
  'admin info' => 'cmcm_level_renewals_admin_info',
  'defaults' => array(
    'height' => '',
    'color_series' => '',
    'display_labels' => 0,
    'include_legend' => 0,
    'include_tooltip' => 0,
  ),
  'all contexts' => TRUE,
);

/**
 * Admin info.
 */
function cmcm_level_renewals_admin_info($subtype, $conf, $contexts) {
  $block = new stdClass();
  $block->title = t('Membership Level: Renewals');
  $block->content = t('Displays information about which part of members of this type get renewed.');

  return $block;
}

/**
 * Settings form.
 */
function cmcm_level_renewals_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['include_legend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display a legend'),
    '#description' => t('Check this box to include a legend in your chart.'),
    '#default_value' => !empty($conf['include_legend']) ? $conf['include_legend'] : 0,
    '#return_value' => 1,
  );

  $form['include_tooltip'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display tooltips'),
    '#description' => t('Check this box to display tooltips when hovering over a point in your chart.'),
    '#default_value' => !empty($conf['include_tooltip']) ? $conf['include_tooltip'] : 0,
    '#return_value' => 1,
  );

  // Display settings.
  $form['display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );

  // Height.
  $form['display_settings']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Please enter the height of the chart, as an integer.'),
    '#default_value' => !empty($conf['height']) ? $conf['height'] : '',
  );

  // Display labels.
  $form['display_labels'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display labels'),
    '#description' => t('Check this box to display labels for every region on your chart.'),
    '#default_value' => !empty($conf['display_labels']) ? $conf['display_labels'] : 0,
    '#return_value' => 1,
  );

  // Color series.
  $form['display_settings']['color_series'] = array(
    '#type' => 'textarea',
    '#title' => t('Color series'),
    '#description' => t('Please enter the colors to use for the data series in valid #RRGGBB or rgba format. Please separate each value with a new line. Leave blank to use the default colors.'),
    '#default_value' => !empty($conf['color_series']) ? $conf['color_series'] : '',
  );

  return $form;
}

/**
 * Validate handler for settings form.
 */
function cmcm_level_renewals_form_validate($form, &$form_state) {

  // Height.
  if (!empty($form_state['values']['height']) && !is_numeric($form_state['values']['height'])) {
    form_set_error('height', t('The height of the chart must be entered as a number (i.e. 200).'));
  }
}

/**
 * Submit handler for settings form.
 */
function cmcm_level_renewals_form_submit($form, &$form_state) {
  // Legend.
  $form_state['conf']['include_legend'] = $form_state['values']['include_legend'];

  // Display tooltip.
  $form_state['conf']['include_tooltip'] = $form_state['values']['include_tooltip'];

  // Height.
  $form_state['conf']['height'] = $form_state['values']['height'];

  // Colors.
  $form_state['conf']['color_series'] = $form_state['values']['color_series'];

  // Display labels.
  $form_state['conf']['display_labels'] = $form_state['values']['display_labels'];
}

/**
 * Render callback.
 */
function cmcm_level_renewals_render($subtype, $conf, $panel_args, $context = NULL) {
  $membership_type_id = $context['argument_entity_id:crm_core_membership_type_1']->argument;
  $rows = array();
  $header = array('Status', 'Percentage');
  $source_data = array();

  // Expired memberships.
  $states = crm_core_membership_states();
  $state = $states['cmcm_expired'];
  $efq = new EntityFieldQuery();
  $expired = $efq
    ->entityCondition('entity_type', 'crm_core_activity')
    ->entityCondition('bundle', 'cmcm_membership')
    ->fieldCondition('field_cmcm_membership_type', 'target_id', $membership_type_id)
    ->propertyCondition('state', $state)
    ->count()
    ->execute();

  // Renewed.
  $states = crm_core_membership_states();
  $state = $states['cmcm_renewed'];
  $efq = new EntityFieldQuery();
  $renewed = $efq
    ->entityCondition('entity_type', 'crm_core_activity')
    ->entityCondition('bundle', 'cmcm_membership')
    ->fieldCondition('field_cmcm_membership_type', 'target_id', $membership_type_id)
    ->propertyCondition('state', $state)
    ->count()
    ->execute();


  // Total.
  $efq = new EntityFieldQuery();
  $total = $efq
    ->entityCondition('entity_type', 'crm_core_activity')
    ->entityCondition('bundle', 'cmcm_membership')
    ->fieldCondition('field_cmcm_membership_type', 'target_id', $membership_type_id)
    ->count()
    ->execute();
  if (empty($total)) {
    $block = new stdClass();
    $block->title = t('Renewals');
    $vars = array(
      'rows' => $rows,
      'header' => $header,
      'empty' => t('There is no members of this membership level.'),
    );
    $block->content = '<div class="table-container">' . theme('table', $vars) . '</div>';;
    return $block;
  }

  // Chart data for renewed.
  $percents = floor(($renewed / $total) * 100);
  // Populate the chart.
  $temp = new flotData(array(array(0, $percents)));
  $temp->label = t('@percents of members who registered at this level choose to renew.', array(
    '@percents' => $percents,
  ));
  $source_data[] = $temp;

  // Populate the table.
  $rows[] = array(
    'data' => array(
      array(
        'data' => t('Renew'),
        'class' => 'table-field-renew',
      ),
      array(
        'data' => $percents . '%',
        'class' => 'table-field-renew-percent',
      ),
    ),
  );

  // Chart data for expired.
  $percents = floor(($expired / $total) * 100);
  // Populate the chart.
  $temp = new flotData(array(array(0, $percents)));
  $temp->label = t('@percents of members who registered at this level choose not to renew.', array(
    '@percents' => $percents,
  ));
  $source_data[] = $temp;

  // Populate the table.
  $rows[] = array(
    'data' => array(
      array(
        'data' => t('Expired'),
        'class' => 'table-field-expired',
      ),
      array(
        'data' => $percents . '%',
        'class' => 'table-field-expired-percent',
      ),
    ),
  );

  // Configuration.
  // If setting not set for widget, we grabbing it from modules configuration.
  $height = empty($conf['height']) ? variable_get('crm_core_event_height', '200') : $conf['height'];
  $color_series = empty($conf['color_series']) ? variable_get('crm_core_event_series_colors', '') : $conf['color_series'];
  $display_labels = empty($conf['display_labels']) ? variable_get('crm_core_event_display_labels', 0) : $conf['display_labels'];
  $include_legend = empty($conf['include_legend']) ? variable_get('crm_core_event_include_legend', 0) : $conf['include_legend'];
  $include_tooltip = empty($conf['include_tooltip']) ? variable_get('crm_core_event_include_tooltips', 0) : $conf['include_tooltip'];

  // Create a new pie chart.
  $options = new flotStylePie();

  // Create a radius, make it a donut chart.
  $options->series->pie->radius = 1;
  $options->series->pie->innerRadius = 0.6;
  $options->series->useLabel = TRUE;
  $options->series->suffix = '%';

  // Add the color series.
  if (!empty($color_series)) {
    $options->colors = explode("\n", $color_series);
  }

  // Add the labels.
  if (!empty($display_labels)) {
    $options->series->pie->label->show = TRUE;
    $options->series->pie->label->radius = 2 / 3;
    $options->series->pie->label->threshold = 0.01;
    $options->series->pie->label->backgroundOpacity = 1;
  }
  else {
    $options->series->pie->label->show = FALSE;
  }

  // Add the tooltips.
  if (!empty($include_tooltip)) {
    $options->series->show_tooltip = TRUE;
  }
  else {
    $options->series->show_tooltip = FALSE;
  }

  // Display the chart.
  $content = theme('flot_graph', array(
    'data' => $source_data,
    'options' => $options,
    'element' => array(
      'style' => 'width: 100%; height: ' . (int) $height . 'px;',
    ),
    'legend' => ($include_legend === 1) ? TRUE : FALSE,
  ));

  // Output the table.
  // Adding wrapper block for fancy styling.
  $content .= '<div class="table-container">' . theme('table', array('rows' => $rows, 'header' => $header)) . '</div>';


  // Return everything.
  $block = new stdClass();
  $block->title = t('Renewals');
  $block->content = $content;

  return $block;
}
