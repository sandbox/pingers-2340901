<?php
/**
 * @file
 * @todo: lifetime option for term
 *  Pages for administrative settings in CRM Core membership.
 */

/**
 * Form callback: create or edit a membership type.
 *
 * @param object $membership_type
 *   (Optional) The membership type object to edit.
 *   Can be empty when used for a create form.
 */
function crm_core_membership_type_form($form, &$form_state, $membership_type) {
  // Store the initial product type in the form state.
  $form_state['membership_type'] = $membership_type;

  $form['membership_type'] = array(
    '#tree' => TRUE,
  );

  $form['membership_type']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $membership_type->enabled,
  );

  $form['membership_type']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $membership_type->name,
    '#description' => t('The human-readable name of this membership type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#required' => TRUE,
    '#size' => 32,
    '#field_suffix' => ' <small id="edit-product-type-name-suffix">' . $membership_type->type . '</small>',
  );

  if (empty($membership_type->type)) {
    $js_settings = array(
      'type' => 'setting',
      'data' => array(
        'machineReadableValue' => array(
          'membership-type-name' => array(
            'text' => t('Machine name'),
            'target' => 'product-type-type',
            'searchPattern' => '[^a-z0-9]+',
            'replaceToken' => '_',
          ),
        ),
      ),
    );

    $form['membership_type']['type'] = array(
      '#type' => 'machine_name',
      '#title' => t('Machine name'),
      '#default_value' => $membership_type->type,
      '#maxlength' => 32,
      '#required' => TRUE,
      '#description' => t('The machine-readable name of this membership type. This name must contain only lowercase letters, numbers, and underscores, it must be unique.'),
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'system') . '/system.js',
          $js_settings,
        ),
      ),
      '#machine_name' => array(
        'exists' => 'crm_core_membership_type_load_by_name',
        'source' => array('membership_type', 'name'),
      ),
    );
  }

  $form['membership_type']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Describe this membership type. The text will be displayed on the <em>Add new content</em> page.'),
    '#default_value' => $membership_type->description,
    '#rows' => 3,
  );

  $form['membership_type']['term'] = array(
    '#type' => 'interval',
    '#title' => 'Term',
    '#description' => t('Enter the amount of time a membership of this level is valid for. Enter a number and select the unit of measurement.'),
    '#default_value' => array(
      'period' => $membership_type->term_period,
      'interval' => $membership_type->term_interval,
    ),
  );

  $form['membership_type']['grace'] = array(
    '#type' => 'interval',
    '#title' => 'Grace period',
    '#description' => t('Enter the length of the grace period for the membership to remain active after the term has finished.'),
    '#default_value' => array(
      'period' => $membership_type->grace_period,
      'interval' => $membership_type->grace_interval,
    ),
  );

  $form['membership_type']['price_new'] = array(
    '#type' => 'textfield',
    '#title' => 'Price for new memberships',
    '#field_prefix' => '$',
    '#default_value' => $membership_type->price_new,
  );

  $form['membership_type']['price_renew'] = array(
    '#type' => 'textfield',
    '#title' => 'Price for renewing existing memberships',
    '#field_prefix' => '$',
    '#default_value' => $membership_type->price_renew,
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save membership type'),
    '#submit' => $submit + array('crm_core_membership_type_form_submit'),
  );

  $form['#validate'][] = 'crm_core_membership_type_form_validate';

  return $form;
}

/**
 * Validation callback.
 */
function crm_core_membership_type_form_validate($form, &$form_state) {
  $membership_type =& $form_state['membership_type'];

  // If saving a new membership type, ensure it has a unique machine name.
  if (empty($membership_type->id)) {
    if (crm_core_membership_type_load_by_name($form_state['values']['membership_type']['type'])) {
      form_set_error('membership_type][type', 'The machine name specified is already in use.');
    }
  }
}

/**
 * Form submit handler: save a membership type.
 */
function crm_core_membership_type_form_submit($form, &$form_state) {
  $membership_type =& $form_state['membership_type'];
  $updated = !empty($membership_type->type);
  $schema = drupal_get_schema('crm_core_membership_type');

  // Fill values from input.
  foreach (array_keys($schema['fields']) as $key) {
    if (array_key_exists($key, $form_state['values']['membership_type'])) {
      $membership_type->{$key} = $form_state['values']['membership_type'][$key];
    }
  }

  $membership_type->term_interval = $form_state['values']['membership_type']['term']['interval'];
  $membership_type->term_period = $form_state['values']['membership_type']['term']['period'];
  $membership_type->grace_interval = $form_state['values']['membership_type']['grace']['interval'];
  $membership_type->grace_period = $form_state['values']['membership_type']['grace']['period'];

  // Write the membership type to the database.
  $membership_type->is_new = !$updated;
  entity_save('crm_core_membership_type', $membership_type);
  drupal_set_message(t('Membership type saved.'));

  // Redirect to the membership overview page.
  $form_state['redirect'] = 'admin/structure/crm-core/membership-types/';
}

/**
 * CRM Core Membership status mapping report page callback.
 */
function crm_core_membership_mapping_report() {
  $content = array(
    'header' => array(
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => t('Creating an activity for a membership will cause the status of the membership to change. You can change what the memership status will be for each activity type by changing the setting on the activity type administration page.'),
      '#attributes' => array(
        'class' => array('crm-core-membership-report-header'),
      ),
    ),
  );

  $rows = array();
  $states = crm_core_membership_states();
  foreach ($states as $bundle => $state) {
    $activity_type = crm_core_activity_type_load($bundle);
    $activity_type_path = 'admin/structure/crm-core/activity-types/manage/' . $bundle;
    if ($activity_type) {
      $rows[] = array(
        l(check_plain($activity_type->label()), $activity_type_path),
        check_plain($state),
      );
    }
  }

  $header = array(
    t('CRM Core Activity Type'),
    t('Membership status'),
  );

  $content['mapping'] = array(
    '#theme' => 'table',
    '#empty' => t('There is no mapping created currently.'),
    '#rows' => $rows,
    '#header' => $header,
  );

  return $content;
}
