<?php

/**
 * @file
 * Hooks exposed by CRM Core Membership module.
 */

/**
 * Allows to alter states mapping.
 *
 * @param array $states
 *   Keys is activity machine name and values is the state names.
 * @param bool $active
 *   TRUE when requested only active membership states.
 *
 * @see crm_core_membership_states()
 */
function hook_crm_core_membership_states_alter(&$states, $active) {

}
