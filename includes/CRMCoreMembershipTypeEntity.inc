<?php

/**
 * CRM Membership Type Entity Class.
 */
class CRMCoreMembershipTypeEntity extends Entity {

  /**
   * {@inheritdoc}
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return array(
      'path' => 'crm-core/membership-type/' . $this->identifier(),
      'options' => array(
        'absolute' => TRUE,
      ),
    );
  }

  /**
   * Checks if membership type with this 'type' property exists.
   *
   * @return bool
   *   TRUE if not exists, FALSE otherwise.
   */
  public function isUnique() {
    $existing = db_select('crm_core_membership_type', 'm')
      ->condition('type', $this->type)
      ->fields('m', array('type'))
      ->execute()
      ->fetchAssoc();
    return !(bool) $existing;
  }
}
