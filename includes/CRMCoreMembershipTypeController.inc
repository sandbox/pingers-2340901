<?php

/**
 * @file
 * Controller class for memberships.
 *
 * This extends the DrupalDefaultEntityController class, adding required
 * special handling for membership objects.
 */

class CRMCoreMembershipTypeController extends EntityAPIController {
  /**
   * Create a basic membership type object.
   */
  public function create(array $values = array()) {
    $values += array(
      'id' => '',
      'enabled' => TRUE,
      'name' => '',
      'type' => '',
      'description' => '',
      'term_interval' => 1,
      'term_period' => 'year',
      'grace_interval' => 1,
      'grace_period' => 'month',
      'price_new' => 0,
      'price_renew' => 0,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  /**
   * Updates 'changed' property on save.
   */
  public function save($entity) {
    $entity->changed = REQUEST_TIME;

    $ret = parent::save($entity);
    $products = array('new', 'renew');
    foreach ($products as $product_type) {
      $sku = _crm_core_membership_get_product_sku($entity->type, $product_type);
      $title = $entity->label();
      $title .= ($product_type == 'new') ? ' product' : ' renew product';
      $price_field = 'price_' . $product_type;
      $price = $entity->{$price_field};

      $product = commerce_product_load_by_sku($sku);
      if ($product) {
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
        $product_wrapper->title = $title;
        $product_wrapper->commerce_price->amount = $price * 100;
      }
      else {
        $new_product = new stdClass();
        $new_product->type = CRM_CORE_MEMBERSHIP_PRODUCT_TYPE;
        $product_wrapper = entity_metadata_wrapper('commerce_product', $new_product);
        $product_wrapper->sku = $sku;

        $product_wrapper->title = $title;
        $product_wrapper->uid = 1;
        $product_wrapper->status = 1;
        $product_wrapper->commerce_price->amount = $price * 100;
      }
      $product_wrapper->save();
    }

    return $ret;
  }

  /**
   * Overridden to care about created product entities.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $product_ids = array();

    foreach ($ids as $id) {
      $products = array('new', 'renew');
      foreach ($products as $product_type) {
        $membership = crm_core_membership_type_load($id);
        $sku = _crm_core_membership_get_product_sku($membership->type, $product_type);
        $product = commerce_product_load_by_sku($sku);
        if ($product) {
          $product_ids[] = $product->product_id;
        }
      }
    }

    if (!empty($product_ids)) {
      commerce_product_delete_multiple($product_ids);
    }

    parent::delete($ids, $transaction);
  }
}
