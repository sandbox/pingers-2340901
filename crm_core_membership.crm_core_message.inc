<?php

/**
 * @file
 * CRM Core Message integration.
 */

/**
 * Implements hook_crm_core_message_events_info().
 */
function crm_core_membership_crm_core_message_events_info() {
  $events = array();

  foreach (crm_core_membership_state_activities() as $activity_bundle) {
    $activity_type = crm_core_activity_type_load($activity_bundle);
    $events[$activity_bundle] = array(
      'label' => $activity_type->label(),
      'module' => 'crm_core_membership',
      'variables' => array(
        'contact' => array(
          'label' => t('Contact'),
          'type' => 'crm_core_contact',
        ),
        'event_activity' => array(
          'label' => t('Event activity'),
          'type' => 'crm_core_activity',
        ),
        'membership_activity' => array(
          'label' => t('Membership activity'),
          'type' => 'crm_core_activity',
        ),
      ),
    );
    if (field_info_instance('crm_core_activity', CRM_CORE_MEMBERSHIP_ORDER_FIELD_NAME, $activity_bundle)) {
      $events[$activity_bundle]['variables']['order'] = array(
        'label' => t('Commerce order'),
        'type' => 'commerce_order',
      );
    }
  }

  return $events;
}
