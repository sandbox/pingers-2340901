<?php
/**
 * @file
 * crm_core_membership.features.inc
 */

/**
 * Implements hook_default_crm_core_activity_type().
 */
function crm_core_membership_default_crm_core_activity_type() {
  $items = array();
  $items['cmcm_cancelled'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_cancelled",
    "label" : "Membership cancelled",
    "weight" : 0,
    "activity_string" : "membership has been cancelled",
    "description" : "Indicates a membership has been cancelled."
  }');
  $items['cmcm_created'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_created",
    "label" : "Membership created",
    "weight" : 0,
    "activity_string" : "has created a membership",
    "description" : "Indicates a member has joined."
  }');
  $items['cmcm_expired'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_expired",
    "label" : "Membership expired",
    "weight" : 0,
    "activity_string" : "membership has expired",
    "description" : "Indicates a member has allowed their membership to expire."
  }');
  $items['cmcm_grace'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_grace",
    "label" : "Membership grace",
    "weight" : 0,
    "activity_string" : "membership is in a grace period",
    "description" : "Indicates a member will expire if it is not renewed."
  }');
  $items['cmcm_membership'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_membership",
    "label" : "Membership",
    "weight" : 0,
    "activity_string" : "has applied for membership",
    "description" : "The membership activity stores current information about a contact\\u0027s membership."
  }');
  $items['cmcm_renewed'] = entity_import('crm_core_activity_type', '{
    "type" : "cmcm_renewed",
    "label" : "Membership renewed",
    "weight" : 0,
    "activity_string" : "has renewed their membership",
    "description" : "Indicates a member has renewed their membership."
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function crm_core_membership_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function crm_core_membership_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
