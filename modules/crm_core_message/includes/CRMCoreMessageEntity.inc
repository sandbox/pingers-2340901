<?php

/**
 * CRM Message Configuration Entity Class.
 */
class CRMCoreMessageEntity extends Entity {

  /**
   * CRM Core Message event name.
   *
   * @var string
   */
  public $event;

  /**
   * Message status.
   *
   * @var bool
   */
  public $enabled;

  /**
   * Event variables.
   *
   * Must be set with proper values before sending the message.
   *
   * @var array
   */
  protected $eventVariables;

  /**
   * Event variables information.
   *
   * @var array
   */
  protected $eventVariablesInfo;

  /**
   * Message fields that should be processed by replaceTokens() method.
   *
   * @var array
   */
  public static $fields = array(
    'from_address',
    'to_address',
    'subject',
    'body_html',
    'body_plain',
  );


  /**
   * {@inheritdoc}
   */
  protected function defaultLabel() {
    return 'Message ' . $this->identifier();
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return array(
      'path' => 'crm-core/message/' . $this->identifier(),
      'options' => array(
        'absolute' => TRUE,
      ),
    );
  }

  /**
   * Returns event variables array.
   *
   * @return array
   *   Event variables.
   */
  public function getEventVariables() {
    return $this->eventVariables;
  }

  /**
   * Set event variables.
   *
   * @param array $variables
   *   Event variables.
   */
  public function setEventVariables($variables) {
    if ($this->validateEventVariables($variables)) {
      $this->eventVariables = $variables;
    }
  }

  /**
   * Get value of single event variable by name.
   *
   * @return array
   *   Event variable value.
   */
  public function getEventVariableByName($name) {
    return isset($this->eventVariables[$name]) ? $this->eventVariables[$name] : NULL;
  }

  /**
   * Get information about event variables.
   *
   * @return array
   *   Event variables information.
   */
  public function getEventVariablesInfo() {
    if (empty($this->eventVariablesInfo)) {
      $this->setEventVariablesInfo();
    }

    return $this->eventVariablesInfo;
  }

  /**
   * Set information about event variables.
   */
  public function setEventVariablesInfo() {
    $events = crm_core_message_get_events();
    $this->eventVariablesInfo = $events[$this->getEvent()]['variables'];
  }

  /**
   * Validates event variables.
   *
   * @todo Implements this.
   *
   * @param array $variables
   *   Array of event variables.
   *
   * @return bool
   *   TRUE if variables are valid, FALSE otherwise.
   */
  public function validateEventVariables($variables) {
    return TRUE;
  }

  /**
   * Get event this message should be triggered by.
   *
   * @return string|NULL
   *   Event string.
   */
  public function getEvent() {
    return empty($this->event) ? NULL : $this->event;
  }

  /**
   * Set event this message should be triggered by.
   *
   * @param string $event
   *   Event name.
   *
   * @throws CRMCoreMessageEntityException
   */
  public function setEvent($event) {
    $events = crm_core_message_get_events();
    if (isset($events[$event])) {
      $this->event = $event;
    }
    else {
      $message = t(
        'Trying to set undefined event on CRM Core Message Entity. Event name provided: "@event".',
        array('@event' => $event)
      );
      throw new CRMCoreMessageEntityException($message);
    }
  }

  /**
   * Replace tokens in message with corresponding values.
   *
   * Loosely based on RulesTokenEvaluator::evaluate().
   *
   * @throws RulesEvaluationException
   * @see RulesTokenEvaluator::evaluate()
   */
  public function replaceTokens() {
    module_load_include('inc', 'rules', 'modules/system.eval');
    // Storing original with unprocessed values.
    $this->origin = $this;

    foreach (self::$fields as $field) {
      $text = $this->{$field};
      $var_info = $this->getEventVariablesInfo();
      $options = array('sanitize' => FALSE);

      $replacements = array();
      // We also support replacing tokens in a list of textual values.
      $whole_text = is_array($text) ? implode('', $text) : $text;
      foreach (token_scan($whole_text) as $var_name => $tokens) {
        $var_name = str_replace('-', '_', $var_name);
        if (isset($var_info[$var_name]) && ($token_type = _rules_system_token_map_type($var_info[$var_name]['type']))) {
          // We have to key $data with the type token uses for the variable.
          $data = rules_unwrap_data(array($token_type => $this->getEventVariableByName($var_name)), array($token_type => $var_info[$var_name]));
          $replacements += token_generate($token_type, $tokens, $data, $options);
        }
        else {
          $replacements += token_generate($var_name, $tokens, array(), $options);
        }
        // Remove tokens if no replacement value is found. As token_replace()
        // does if 'clear' is set.
        $replacements += array_fill_keys($tokens, '');
      }

      // Actually apply the replacements.
      $tokens = array_keys($replacements);
      $values = array_values($replacements);
      if (is_array($text)) {
        foreach ($text as $i => $text_item) {
          $text[$i] = str_replace($tokens, $values, $text_item);
        }
        $this->{$field} = $text;
      }
      $this->{$field} = str_replace($tokens, $values, $text);
    }

  }

  /**
   * Message send callback.
   *
   * Based on rules_action_mimemail().
   */
  public function send() {
    module_load_include('inc', 'mimemail');
    $this->replaceTokens();

    $to = $this->to_address;
    $key = 'crm_core_message_' . $this->identifier();

    $from = array(
      'name' => NULL,
      'mail' => $this->from_address,
    );
    // Create an address string.
    $from = mimemail_address($from);

    $params = array(
      'context' => array(
        'subject' => $this->subject,
        'body' => $this->body_html,
      ),
      'plaintext' => empty($this->body_plain) ? NULL : $this->body_plain,
      'attachments' => '',
    );

    drupal_mail('mimemail', $key, $to, language_default(), $params, $from);
  }

  /**
   * Message enabled.
   *
   * @return bool
   *   Message enabled.
   */
  public function isEnabled() {
    return !empty($this->enabled);
  }

  /**
   * Set message status.
   *
   * @param bool $enabled
   *   Status.
   */
  public function setEnabled($enabled) {
    $this->enabled = boolval($enabled);
  }

}

class CRMCoreMessageEntityException extends Exception {}
