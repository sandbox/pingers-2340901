<?php

/**
 * @file
 * Controller class for memberships.
 *
 * This extends the DrupalDefaultEntityController class, adding required
 * special handling for membership objects.
 */

class CRMCoreMessageController extends EntityAPIControllerExportable {
  /**
   * Create a basic membership type object.
   */
  public function create(array $values = array()) {
    $values += array(
      'id' => '',
      'module' => '',
      'event' => '',
      'enabled' => 1,
      'from_address' => '',
      'to_address' => '',
      'subject' => '',
      'body_plain' => '',
      'body_html' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  /**
   * Updates 'changed' property on save.
   */
  public function save($entity) {
    $entity->changed = REQUEST_TIME;

    return parent::save($entity);
  }
}
