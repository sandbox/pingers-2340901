<?php
/**
 * @file
 * Pages for administrative settings in CRM Core Message.
 */

/**
 * Message add/edit form builder.
 *
 * @param object $message
 *   The message object to edit or result of entity_create().
 */
function crm_core_message_form($form, &$form_state, $message) {
  /* @var $message CRMCoreMessageEntity */
  // Store the initial product type in the form state.
  $form_state['message'] =& $message;

  $form['message'] = array(
    '#tree' => TRUE,
  );

  $form['message']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $message->enabled,
  );

  $form['message']['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#default_value' => $message->module,
    '#description' => t('Module that this message belongs to.'),
    '#required' => TRUE,
    '#size' => 32,
    '#access' => FALSE,
  );

  $event = NULL;
  if (empty($form_state['values']['message']['event'])) {
    $event = $message->getEvent();
  }
  else {
    $event = $form_state['values']['message']['event'];
  }

  $form['message']['event'] = array(
    '#type' => 'select',
    '#title' => t('Event'),
    '#default_value' => $event,
    '#empty_option' => t('--Please select--'),
    '#empty_value' => '',
    '#description' => t('Event that triggers this message.'),
    '#required' => TRUE,
    '#options' => crm_core_message_get_event_options(),
    '#ajax' => array(
      'wrapper' => 'message-tokens-wrapper',
      'callback' => '_crm_core_message_tokens_ajax_callback',
    ),
  );

  if ($event) {
    $var_info = array(
      'site' => array(
        'type' => 'site',
        'label' => t('Site information'),
        'description' => t('Site-wide settings and other global information.'),
      ),
    );

    $event_info = crm_core_message_get_event_info($event);
    $var_info += $event_info['variables'];

    $form['message']['tokens'] = RulesTokenEvaluator::help($var_info);
    $form['message']['tokens']['#prefix'] = '<div id="message-tokens-wrapper">';
    $form['message']['tokens']['#suffix'] = '</div>';
  }
  else {
    // We need a place to place tokens selector on add form.
    $form['message']['event']['#suffix'] = '<div id="message-tokens-wrapper"></div>';
  }

  $form['message']['from_address'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#default_value' => $message->from_address,
    '#description' => t('From email address.'),
    '#required' => TRUE,
    '#size' => 128,
  );

  $form['message']['to_address'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#default_value' => $message->to_address,
    '#description' => t('From email address.'),
    '#required' => TRUE,
    '#size' => 128,
  );

  $form['message']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $message->subject,
    '#description' => t('Message subject.'),
    '#required' => TRUE,
    '#size' => 128,
  );

  $form['message']['body_html'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => $message->body_html,
    '#description' => t('Message body.'),
    '#required' => TRUE,
  );

  $form['message']['body_plain'] = array(
    '#type' => 'textarea',
    '#title' => t('Body (plain text)'),
    '#default_value' => $message->body_plain,
    '#description' => t('Plain text body for robots and limited functionality email clients. This can be left empty'),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save message'),
    '#submit' => $submit + array('crm_core_message_form_submit'),
  );

  return $form;
}

/**
 * CRM Core Message form submit handler.
 */
function crm_core_message_form_submit($form, &$form_state) {
  $message =& $form_state['message'];
  /* @var $message CRMCoreMessageEntity */
  // Fill values from input.
  $schema = drupal_get_schema('crm_core_message');
  foreach (array_keys($schema['fields']) as $key) {
    if (array_key_exists($key, $form_state['values']['message'])) {
      if ($key == 'event') {
        $message->setEvent($form_state['values']['message'][$key]);
      }
      else {
        $message->{$key} = $form_state['values']['message'][$key];
      }
    }
  }

  // Write the membership type to the database.
  entity_save('crm_core_message', $message);

  // Redirect based on the button clicked.
  $form_state['redirect'] = 'admin/structure/crm-core/messages/';
  drupal_set_message(t('Message @label has been saved.', array('@label' => $message->label())));
}

/**
 * AJAX callback.
 */
function _crm_core_message_tokens_ajax_callback($form, $form_state) {
  return $form['message']['tokens'];
}
