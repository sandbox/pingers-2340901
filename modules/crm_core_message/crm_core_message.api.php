<?php
/**
 * @file
 * CRM Core Message API.
 *
 * To simplify the configuration UI around sending HTML messages from the
 * Drupal-system we are going to mimic Rules events system that will allow
 * users to create a message that will be triggered by specific event.
 * As the email sending backend we will use MIME Mail module functionality
 * based on the "Send HTML e-mail" action.
 */

/**
 * Provide info about the events provided by the module.
 *
 * This system is simplified version of the rules events and try's to reuse it
 * as much as possible.
 * The module has to invoke the event when it occurs
 * using crm_core_message_invoke_event().
 * Usually it's invoked directly from the providing module, but wrapped by a
 * module_exists('crm_core_message') check.
 *
 * @return array
 *   An array of information about the module's provided events. The array
 *   contains a sub-array for each event, with the event name as the key. The
 *   name may only contain lower case alpha-numeric characters and underscores
 *   and should be prefixed with the providing module name. Possible attributes
 *   for each sub-array are:
 *   - label: The label of the event. Start capitalized. Required.
 *   - variables: An array describing all variables that are
 *     available for elements reacting on this event. Each variable has to be
 *     described by a sub-array with the possible attributes:
 *     - label: The label of the variable. Start capitalized. Required.
 *     - type: The rules data type of the variable. All types declared in
 *       hook_rules_data_info() or supported by hook_entity_property_info() may
 *       be specified.
 *
 * @see hook_rules_event_info()
 * @see rules_invoke_event()
 * @see crm_core_message_get_events()
 */
function hook_crm_core_message_events_info() {
  $events = array(
    'event_name' => array(
      'label' => t('Event label'),
      'module' => 'module_name',
      'variables' => array(
        'variable_name' => array(
          'label' => t('Variable label'),
          'type' => 'var_type',
        ),
      ),
    ),
  );

  return $events;
}

function hook_crm_core_message_events_info_alter(&$events) {
  $events['some_module']['some_event']['variables']['some_var']['label'] = t('New label');
}
