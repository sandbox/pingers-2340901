<?php

/**
 * @file
 * Install, update and uninstall functions for the CRM Core message module.
 */

/**
 * Implements hook_schema().
 */
function crm_core_message_schema() {
  $schema['crm_core_message'] = array(
    'description' => 'Stores information about message configuration entities.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique message ID.',
      ),
      'module' => array(
        'description' => 'Module this message entity belongs to.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'event' => array(
        'description' => 'Event name(i.e. crm_core_membership_created).',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'enabled' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
        'size' => 'tiny',
        'description' => 'Message status: 1 - enabled, 0 - disabled.',
      ),
      'from_address' => array(
        'description' => 'From email address.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'to_address' => array(
        'description' => 'To email address.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'subject' => array(
        'description' => 'Message subject.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
      ),
      'body_plain' => array(
        'description' => 'Plain text body',
        'type' => 'text',
        'size' => 'small',
        'not null' => FALSE,
        'translatable' => TRUE,
      ),
      'body_html' => array(
        'description' => 'HTML text body',
        'type' => 'text',
        'size' => 'medium',
        'not null' => FALSE,
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
  );

  return $schema;
}
