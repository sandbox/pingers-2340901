<?php
/**
 * @file
 * crm_core_membership.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function crm_core_membership_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'cmcm_membership_level_report';
  $page->task = 'page';
  $page->admin_title = 'Membership Level Summary Report';
  $page->admin_description = 'Summary report for membership levels in CRM Core Membership.';
  $page->path = 'crm-core/reports/cmcm/levels/summary/%level';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'view membership reports',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'action',
    'title' => 'Detail',
    'name' => 'navigation',
    'weight' => '-9',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'level' => array(
      'id' => 1,
      'identifier' => 'Level',
      'name' => 'entity_id:crm_core_membership_type',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_cmcm_membership_level_report_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'cmcm_membership_level_report';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
          1 => 1,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'right',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '202e5e1d-5a37-4498-b683-3ff2d306e324';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9ba773af-b252-4d7a-8342-ed20704f4173';
    $pane->panel = 'center';
    $pane->type = 'cmcm_overview_metrics';
    $pane->subtype = 'cmcm_overview_metrics';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9ba773af-b252-4d7a-8342-ed20704f4173';
    $display->content['new-9ba773af-b252-4d7a-8342-ed20704f4173'] = $pane;
    $display->panels['center'][0] = 'new-9ba773af-b252-4d7a-8342-ed20704f4173';
    $pane = new stdClass();
    $pane->pid = 'new-cf368a3a-6ebd-4902-99bc-a57e14e2e3c1';
    $pane->panel = 'center';
    $pane->type = 'cmcm_level_activity_chart';
    $pane->subtype = 'cmcm_level_activity_chart';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'days_to_display' => 'All',
      'height' => '',
      'xaxis_tickmarks' => '',
      'chart_color' => '',
      'line_color' => '',
      'background_color' => '',
      'include_legend' => 0,
      'include_tooltip' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'cf368a3a-6ebd-4902-99bc-a57e14e2e3c1';
    $display->content['new-cf368a3a-6ebd-4902-99bc-a57e14e2e3c1'] = $pane;
    $display->panels['center'][1] = 'new-cf368a3a-6ebd-4902-99bc-a57e14e2e3c1';
    $pane = new stdClass();
    $pane->pid = 'new-5be7b493-5b01-4048-89b5-f9d8e26e915d';
    $pane->panel = 'center';
    $pane->type = 'cmcm_level_activity_table';
    $pane->subtype = 'cmcm_level_activity_table';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'days_to_display' => 'All',
      'height' => '',
      'xaxis_tickmarks' => '',
      'chart_color' => '',
      'line_color' => '',
      'background_color' => '',
      'include_legend' => 0,
      'include_tooltip' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5be7b493-5b01-4048-89b5-f9d8e26e915d';
    $display->content['new-5be7b493-5b01-4048-89b5-f9d8e26e915d'] = $pane;
    $display->panels['center'][2] = 'new-5be7b493-5b01-4048-89b5-f9d8e26e915d';
    $pane = new stdClass();
    $pane->pid = 'new-4f90eb48-3126-45dc-af91-f3892eee4e68';
    $pane->panel = 'left';
    $pane->type = 'cmcm_level_renewals';
    $pane->subtype = 'cmcm_level_renewals';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'height' => '',
      'color_series' => '',
      'display_labels' => 0,
      'include_legend' => 0,
      'include_tooltip' => 1,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4f90eb48-3126-45dc-af91-f3892eee4e68';
    $display->content['new-4f90eb48-3126-45dc-af91-f3892eee4e68'] = $pane;
    $display->panels['left'][0] = 'new-4f90eb48-3126-45dc-af91-f3892eee4e68';
    $pane = new stdClass();
    $pane->pid = 'new-ba0bb1da-c05c-4d9d-a651-64bfcda58383';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Membership Levels by Source',
      'body' => 'This is where we display the chart displaying sources for this membership level',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ba0bb1da-c05c-4d9d-a651-64bfcda58383';
    $display->content['new-ba0bb1da-c05c-4d9d-a651-64bfcda58383'] = $pane;
    $display->panels['right'][0] = 'new-ba0bb1da-c05c-4d9d-a651-64bfcda58383';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['cmcm_membership_level_report'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'crm_core_membership_reports';
  $page->task = 'page';
  $page->admin_title = 'CRM Core Membership Reports';
  $page->admin_description = 'This is the overview page for reports in CRM Core Membership';
  $page->path = 'crm-core/reports/cmcm/overview';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'view membership reports',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'default tab',
    'title' => 'Overview',
    'name' => 'navigation',
    'weight' => '-10',
    'parent' => array(
      'type' => 'normal',
      'title' => 'Membership Reports',
      'name' => 'features',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_crm_core_membership_reports_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'crm_core_membership_reports';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 2,
          1 => 'main-row',
          2 => 1,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'right_',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'right',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      'right_' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => 50,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
        ),
        'parent' => 'main',
        'class' => 'top-outer',
        'hide_empty' => 0,
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'Top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '2',
        'class' => 'top',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'left' => NULL,
      'right' => NULL,
      'right_' => NULL,
      'top' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '095777fa-6802-480e-a30c-0cdd951849e9';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-56c18c00-046a-4a8b-b3ac-5974d2b4b9ae';
    $pane->panel = 'center';
    $pane->type = 'membership_history_chart';
    $pane->subtype = 'membership_history_chart';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'days_to_display' => '180',
      'height' => '300',
      'xaxis_tickmarks' => '',
      'chart_color' => '',
      'line_color' => '',
      'background_color' => '',
      'include_legend' => 0,
      'include_tooltip' => 0,
      'override_title' => 1,
      'override_title_text' => 'New Membership Report',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '56c18c00-046a-4a8b-b3ac-5974d2b4b9ae';
    $display->content['new-56c18c00-046a-4a8b-b3ac-5974d2b4b9ae'] = $pane;
    $display->panels['center'][0] = 'new-56c18c00-046a-4a8b-b3ac-5974d2b4b9ae';
    $pane = new stdClass();
    $pane->pid = 'new-bb243bbd-819a-4feb-8a83-783175f2b8d6';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'cmcm_recent_members_widget-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'use_pager' => 1,
      'pager_id' => '0',
      'items_per_page' => '10',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bb243bbd-819a-4feb-8a83-783175f2b8d6';
    $display->content['new-bb243bbd-819a-4feb-8a83-783175f2b8d6'] = $pane;
    $display->panels['left'][0] = 'new-bb243bbd-819a-4feb-8a83-783175f2b8d6';
    $pane = new stdClass();
    $pane->pid = 'new-4c18d730-8675-44a2-9102-b849301f24c4';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'cmcm_grace_members_widget-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'use_pager' => 1,
      'pager_id' => '0',
      'items_per_page' => '10',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4c18d730-8675-44a2-9102-b849301f24c4';
    $display->content['new-4c18d730-8675-44a2-9102-b849301f24c4'] = $pane;
    $display->panels['right'][0] = 'new-4c18d730-8675-44a2-9102-b849301f24c4';
    $pane = new stdClass();
    $pane->pid = 'new-46bdba89-2ba5-4ee8-a9f8-f74b1d769212';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'cmcm_recently_expired_members_widget-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'use_pager' => 1,
      'pager_id' => '0',
      'items_per_page' => '10',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '46bdba89-2ba5-4ee8-a9f8-f74b1d769212';
    $display->content['new-46bdba89-2ba5-4ee8-a9f8-f74b1d769212'] = $pane;
    $display->panels['right'][1] = 'new-46bdba89-2ba5-4ee8-a9f8-f74b1d769212';
    $pane = new stdClass();
    $pane->pid = 'new-09314e8e-6887-47cd-9d15-613c1e8c2701';
    $pane->panel = 'right_';
    $pane->type = 'cmcm_level_bar_chart_overview';
    $pane->subtype = 'cmcm_level_bar_chart_overview';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'height' => '',
      'color_series' => '',
      'chart_color' => '',
      'column_width' => '',
      'include_legend' => 0,
      'include_tooltip' => 0,
      'override_title' => 1,
      'override_title_text' => 'Membership Levels',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '09314e8e-6887-47cd-9d15-613c1e8c2701';
    $display->content['new-09314e8e-6887-47cd-9d15-613c1e8c2701'] = $pane;
    $display->panels['right_'][0] = 'new-09314e8e-6887-47cd-9d15-613c1e8c2701';
    $pane = new stdClass();
    $pane->pid = 'new-cd8a9f0f-28e7-46e7-95f0-01f823252978';
    $pane->panel = 'top';
    $pane->type = 'cmcm_overview_metrics';
    $pane->subtype = 'cmcm_overview_metrics';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cd8a9f0f-28e7-46e7-95f0-01f823252978';
    $display->content['new-cd8a9f0f-28e7-46e7-95f0-01f823252978'] = $pane;
    $display->panels['top'][0] = 'new-cd8a9f0f-28e7-46e7-95f0-01f823252978';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['crm_core_membership_reports'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'membership_levels';
  $page->task = 'page';
  $page->admin_title = 'Membership Levels';
  $page->admin_description = 'Provides details on CRM Core Membership Levels by type';
  $page->path = 'crm-core/reports/cmcm/levels';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'view membership reports',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Levels',
    'name' => 'navigation',
    'weight' => '-9',
    'parent' => array(
      'type' => 'tab',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_membership_levels_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'membership_levels';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Membership Level Report';
  $display->uuid = '202e5e1d-5a37-4498-b683-3ff2d306e324';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6a0fe63a-d0ff-4445-83f0-8db7d7f73c23';
    $pane->panel = 'center';
    $pane->type = 'cmcm_membership_bar_chart';
    $pane->subtype = 'cmcm_membership_bar_chart';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'height' => '',
      'color_series' => '',
      'chart_color' => '',
      'column_width' => '',
      'include_legend' => 0,
      'include_tooltip' => 0,
      'override_title' => 1,
      'override_title_text' => 'Membership by Level',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6a0fe63a-d0ff-4445-83f0-8db7d7f73c23';
    $display->content['new-6a0fe63a-d0ff-4445-83f0-8db7d7f73c23'] = $pane;
    $display->panels['center'][0] = 'new-6a0fe63a-d0ff-4445-83f0-8db7d7f73c23';
    $pane = new stdClass();
    $pane->pid = 'new-b5270fb3-8ad7-47b1-952c-607d511cdb6f';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'cmcm_levels_overview-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b5270fb3-8ad7-47b1-952c-607d511cdb6f';
    $display->content['new-b5270fb3-8ad7-47b1-952c-607d511cdb6f'] = $pane;
    $display->panels['center'][1] = 'new-b5270fb3-8ad7-47b1-952c-607d511cdb6f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['membership_levels'] = $page;

  return $pages;

}
